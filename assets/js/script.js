(function(){
	var imagens = getArrayCartas();
	var cartasViradas = [];
	var cartasCorretas = 0;
	var modalFimJogo = document.querySelector("#modalFimJogo");
	var imgflagAcertou = document.querySelector("#imgAcertou");
 
	startGame();

	/*Distribui as cartas na tela para a iniação do jogo.  */
	function startGame(){
		cartasCorretas = 0;
		cartasViradas = [];
		imagens = embaralharCartas(imagens);

		var frontFaces = document.getElementsByClassName("front");
		var backFaces = document.getElementsByClassName("back");


		for (var i = 0; i < imagens.length; i++){
			frontFaces[i].classList.remove("flipped", "match");
			backFaces[i].classList.remove("flipped", "match");

			var carta = document.querySelector("#card" + i);
			carta.style.left = getPxLateralPorCarta(i);
			carta.style.top = getPxTopoPorCarta(i);
			carta.addEventListener("click", virarCarta, false);

			adicionarAtributosNaClasseFaceFront(frontFaces, i);
		}


		modalFimJogo.style.zIndex = -2;
		modalFimJogo.removeEventListener("click", startGame, false);
	}

	function adicionarAtributosNaClasseFaceFront(elemento, indice){
		elemento[indice].style.background = "url('" + imagens[indice].src + "')";
		elemento[indice].setAttribute("id", imagens[indice].id);
	}

	/* Cria um array de cartas com seus respectivos ids e src da imagem relacionada.*/
	function getArrayCartas() {
		var cartas = [];
		
		for(var i = 0; i < 28; i++){
			var img = {
				src: "assets/img/" + i + ".png",
				id: i % 14
			};

			cartas.push(img);
		}

		return cartas;
	}

	/* Embaralha as cartas aleatoriamente.*/
	function embaralharCartas(cartas){
		var cartasEmbaralhadas = [];

		while(cartasEmbaralhadas.length != cartas.length){
			var i = Math.floor(Math.random()*cartas.length);

			if(cartasEmbaralhadas.indexOf(cartas[i]) < 0){
				cartasEmbaralhadas.push(cartas[i]);
			}
		}

		return cartasEmbaralhadas;
	}

	/*calculo para margem a esquerda:
		- a primeira carta de cada linha terá 5px;
		- as demais terá:
			i % quantidade de colunas * (largura da carta + 5) + 5px de distanciamento entre cartas
	*/
	function getPxLateralPorCarta(indice) {
		var moduloIndice = indice % 7;
		var valor = moduloIndice === 0 ? 5 : moduloIndice * 125 + 5;
		
		return valor + "px";
	}

	/*calculo para margem  do topo:
		- a primeira linha de carta terá 5px;
		- as demais terão: (altura da carta + 4) * (indice  / quantidade de colunas) + 4 px;

	*/
	function getPxTopoPorCarta(indice) {
		var valor = indice < 7 ? 5 : 154 * Math.floor(indice / 7) + 4;

		return valor + "px";
	}

	
	/*Adiciona a ação de virar as cartas fazendo com que no máximo 2 cartas fiquem viradas. */
	function virarCarta() {
		if (cartasViradas.length < 2) {
			var faces = this.getElementsByClassName("face");

			if (faces[0].classList.length > 2) {
				return;
			}

			toogleClasse(faces[0], "flipped");
			toogleClasse(faces[1], "flipped");
			cartasViradas.push(this);

			if (cartasViradas.length === 2) {
				if (cartasViradas[0].childNodes[3].id === cartasViradas[1].childNodes[3].id){
					toogleClasse(cartasViradas[0].childNodes[1], "match");
					toogleClasse(cartasViradas[0].childNodes[3], "match");

					toogleClasse(cartasViradas[1].childNodes[1], "match");
					toogleClasse(cartasViradas[1].childNodes[3], "match");

					marcarCartasCorretas();
					cartasCorretas++;
					cartasViradas = [];

					if (cartasCorretas === 14){
						fimJogo();
					}
				} else {
					setTimeout(function(){
						toogleClasse(cartasViradas[0].childNodes[1], "flipped");
						toogleClasse(cartasViradas[0].childNodes[3], "flipped");

						toogleClasse(cartasViradas[1].childNodes[1], "flipped");
						toogleClasse(cartasViradas[1].childNodes[3], "flipped");
						
						cartasViradas = [];
					},2000);		
				}
			}
		} 
	}

	function toogleClasse(elemento, classe){
		elemento.classList.toggle(classe);
	}

	function fimJogo() {
		modalFimJogo.style.zIndex = 10;
		modalFimJogo.addEventListener("click", startGame, false);
	}

	function marcarCartasCorretas(){
		imgflagAcertou.style.zIndex = 1;
		imgflagAcertou.style.top = 150 + "px";
		imgflagAcertou.style.opacity = 0;
		
		setTimeout(function(){
			imgflagAcertou.style.zIndex = -1;
			imgflagAcertou.style.top = 250 + "px";
			imgflagAcertou.style.opacity = 1;
		}, 1500);
	}

}());